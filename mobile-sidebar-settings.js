$(document).ready(function () {

    $('.left.sidebar')
        .sidebar({
            dimPage: false,
            exclusive: true,
            transition: 'overlay',
            mobileTransition: 'overlay',
            onShow: function () {
                $('.leftSidebarToggle').addClass('whileSidebarVisible');
                $('.leftSidebarToggle > i').removeClass('sidebar').addClass('close');
            },
            onHidden: function () {
                $('.leftSidebarToggle').removeClass('whileSidebarVisible');
                $('.leftSidebarToggle > i').removeClass('close').addClass('sidebar');
            }
        })
        .sidebar('attach events', '.leftSidebarToggle');

    $('.right.sidebar')
        .sidebar({
            dimPage: false,
            exclusive: true,
            transition: 'overlay',
            mobileTransition: 'overlay',
            onShow: function () {
                $('.rightSidebarToggle').addClass('whileSidebarVisible');
                $('.rightSidebarToggle > i').removeClass('ellipsis vertical').addClass('close');
            },
            onHidden: function () {
                $('.rightSidebarToggle').removeClass('whileSidebarVisible');
                $('.rightSidebarToggle > i').removeClass('close').addClass('ellipsis vertical');
            }
        })
        .sidebar('attach events', '.rightSidebarToggle');

});